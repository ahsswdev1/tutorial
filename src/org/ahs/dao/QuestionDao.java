package org.ahs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ahs.model.Question;
import org.ahs.util.DbUtil;

public class QuestionDao {

    private Connection connection;
 
    public QuestionDao() {
        connection = DbUtil.getConnection();
    }

    public void addQuestion(Question question) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into question(question_number, question_text,image_location) values (?, ?, ? )");
            // Parameters start with 1
            preparedStatement.setInt(1, question.getQuestionNumber());
            preparedStatement.setString(2, question.getQuestionText());
            preparedStatement.setString(3, question.getImageLocation());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteQuestion(int questionId) {
        try {
       	PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from question where id=?");
            // Parameters start with 1
            preparedStatement.setInt(1, questionId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateQuestion(Question question) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update question set question_number = ?, question_text=?, image_location=?" +
                            "where id=?");
            // Parameters start with 1
            preparedStatement.setInt(1, question.getQuestionNumber());
            preparedStatement.setString(2, question.getQuestionText());
            preparedStatement.setString(3, question.getImageLocation());
            preparedStatement.setInt(4, question.getQuestionId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Question> getAllQuestions() {
        List<Question> questions = new ArrayList<Question>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from question");
            while (rs.next()) {
                Question question = new Question();
                question.setQuestionId(rs.getInt("id"));
                question.setQuestionNumber(rs.getInt("question_number"));
                question.setQuestionText(rs.getString("question_text"));
                question.setImageLocation(rs.getString("image_location"));
                questions.add(question);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return questions;
    }

    public Question getQuestionById(int questionId) {
        Question question = new Question();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from question where id=?");
            preparedStatement.setInt(1, questionId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                question.setQuestionId(rs.getInt("id"));
                question.setQuestionNumber(rs.getInt("question_number"));
                question.setImageLocation(rs.getString("image_location"));
                question.setQuestionText(rs.getString("question_text"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return question;
    }
}